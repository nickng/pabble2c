; ModuleID = 'convolution.bc'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:32:64-f32:32:32-f64:32:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32-n8:16:32"
target triple = "i386-pc-linux-gnu"

define i32 @main() nounwind {
  %1 = alloca i32, align 4
  %sampleCount = alloca i32, align 4
  %kernelCount = alloca i32, align 4
  %y = alloca [10 x i32], align 4
  %x = alloca [10 x i32], align 4
  %h = alloca [4 x i32], align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store i32 0, i32* %1
  store i32 10, i32* %sampleCount, align 4
  store i32 4, i32* %kernelCount, align 4
  store i32 0, i32* %i, align 4
  br label %2

; <label>:2                                       ; preds = %31, %0
  %3 = load i32* %i, align 4
  %4 = load i32* %sampleCount, align 4
  %5 = icmp slt i32 %3, %4
  br i1 %5, label %6, label %34

; <label>:6                                       ; preds = %2
  %7 = load i32* %i, align 4
  %8 = getelementptr inbounds [10 x i32]* %y, i32 0, i32 %7
  store i32 0, i32* %8
  store i32 0, i32* %j, align 4
  br label %9

; <label>:9                                       ; preds = %27, %6
  %10 = load i32* %j, align 4
  %11 = load i32* %kernelCount, align 4
  %12 = icmp slt i32 %10, %11
  br i1 %12, label %13, label %30

; <label>:13                                      ; preds = %9
  %14 = load i32* %i, align 4
  %15 = load i32* %j, align 4
  %16 = sub nsw i32 %14, %15
  %17 = getelementptr inbounds [10 x i32]* %x, i32 0, i32 %16
  %18 = load i32* %17
  %19 = load i32* %j, align 4
  %20 = getelementptr inbounds [4 x i32]* %h, i32 0, i32 %19
  %21 = load i32* %20
  %22 = mul nsw i32 %18, %21
  %23 = load i32* %i, align 4
  %24 = getelementptr inbounds [10 x i32]* %y, i32 0, i32 %23
  %25 = load i32* %24
  %26 = add nsw i32 %25, %22
  store i32 %26, i32* %24
  br label %27

; <label>:27                                      ; preds = %13
  %28 = load i32* %j, align 4
  %29 = add nsw i32 %28, 1
  store i32 %29, i32* %j, align 4
  br label %9

; <label>:30                                      ; preds = %9
  br label %31

; <label>:31                                      ; preds = %30
  %32 = load i32* %i, align 4
  %33 = add nsw i32 %32, 1
  store i32 %33, i32* %i, align 4
  br label %2

; <label>:34                                      ; preds = %2
  ret i32 0
}
