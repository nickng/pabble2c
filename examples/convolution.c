int main() {

	int sampleCount = 10;
	int kernelCount = 4;
	int y[10];
	int x[10];
	int h[4];

	int i,j;
	for ( i = 0; i < sampleCount; i++ )
	{
		y[i] = 0;                       // set to zero before sum
		for ( j = 0; j < kernelCount; j++ )
		{
			y[i] += x[i - j] * h[j];    // convolve: multiply and accumulate
		}
	}
	return 0;
}
