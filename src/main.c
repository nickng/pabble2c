#include <stdlib.h>
#include <stdio.h>

#include <sesstype/st_expr.h>
#include <sesstype/st_node.h>
#include <sesstype/st_node_print.h>
#include <sesstype/st_normalise.h>

#include <scribble/parser.h>
#include <scribble/print.h>
#include <scribble/print_utils.h>

extern int yyparse(st_tree *tree);
extern FILE *yyin;
extern int scribble_codegen_mode;

static void C_DEFINE(const char *name, unsigned int value)
{
  printf("#define %s %u\n", name, value);
}

static void C_ARRAY(const char *name, unsigned int size, const char *type)
{
  printf("%s %s[%u];\n", type, name, size);
}

/**
 * Preamble definitions in global scope.
 */
static void preamble_global(st_info *info)
{
  printf("//Protocol %s.\n", info->name);
  // const N = __VALUE__;
  // --> #define N __VALUE__
  for (int c=0; c<info->nconst; c++) {
    switch (info->consts[c]->type) {
      case ST_CONST_VALUE:
        C_DEFINE(info->consts[c]->name, info->consts[c]->value);
        break;
      case ST_CONST_BOUND:
      case ST_CONST_INF:
        fprintf(stderr, "Error [constant %s]: Only value constants supported.",
            info->consts[c]->name);
    }
  }

  if (info->nimport > 0) fprintf(stderr, "Error: imports not supported.");
  if (info->ngroup > 0)  fprintf(stderr, "Error: groups not supported.");
}

/**
 * Preamble definitions in main scope.
 */
static void preamble(st_info *info)
{
  // role __ROLENAME__[__FROM__..__TO__][...]
  // --> int __ROLENAME__[__TO__ - __FROM__][...];
  for (int r=0; r<info->nrole; r++) {

    printf("int %s", info->roles[r]->name);
    for (int d=0; d<info->roles[r]->dimen; d++) {
      assert(info->roles[r]->param[d]->type == ST_EXPR_TYPE_RNG);
      printf("[");
      st_expr_print(info->roles[r]->param[d]->rng->to);
      printf("-");
      st_expr_print(info->roles[r]->param[d]->rng->from);
      printf("+1]");
      // TODO flatten above expression
    }
    printf("; // TODO simplify expression\n");

  }

}

static void _print_as_var(st_role *role)
{
  printf("%s", role->name);
  for (int d=0; d<role->dimen; d++) {
    switch (role->param[d]->type) {
      case ST_EXPR_TYPE_RNG:
        fprintf(stderr, "Error: Cannot print range as variable.");
        break;
      default:
        printf("[");
        st_expr_print(role->param[d]);
        printf("]");
    }
  }
}

static void _gen_par_interaction(st_node *node)
{
  fprintf(stderr, "TODO");
}

static void _gen_interaction(st_node* node)
{
  // Special case of parallel interaction.
  if (   node->interaction->from->dimen > 0
      && node->interaction->from->param[0]->type == ST_EXPR_TYPE_RNG) {
    _gen_par_interaction(node);
  } else {
    _print_as_var(node->interaction->to[0]);
    if (strcmp(node->interaction->msgsig.op, "") == 0) {
      printf(" = ");
    } else if (strcmp(node->interaction->msgsig.op, "__add__") == 0) {
      printf(" += ");
    } else if (strcmp(node->interaction->msgsig.op, "__mul__") == 0) {
      printf(" *= ");
    } else if (strcmp(node->interaction->msgsig.op, "__sub__") == 0) { // Not commutative
      printf(" -= ");
    } else if (strcmp(node->interaction->msgsig.op, "__div__") == 0) { // Not commutative
      printf(" /= ");
    } else {
      fprintf(stderr, "Error: operator %s not supported.", node->interaction->msgsig.op);
    }
    _print_as_var(node->interaction->from);
    printf(";\n");
  }
}

static void gen(st_node *node);
static void _gen_foreach(st_node *node)
{
  // First, define the variable.
  printf("int %s;\n", node->forloop->range->bindvar);
  printf("for (%s=", node->forloop->range->bindvar);
  st_expr_print(node->forloop->range->from);
  printf("; %s<=", node->forloop->range->bindvar);
  st_expr_print(node->forloop->range->to);
  printf("; %s++) {\n", node->forloop->range->bindvar);

  for (int child=0; child<node->nchild; child++) {
    gen(node->children[child]);
  }
  printf("}\n");
}

static void gen(st_node *node)
{
  switch (node->type) {
    case ST_NODE_SENDRECV:
      _gen_interaction(node);
      break;
    case ST_NODE_FOR:
      _gen_foreach(node);
      break;
    case ST_NODE_ROOT:
      for (int child=0; child<node->nchild; child++) {
        gen(node->children[child]);
      }
      break;
    default:
      fprintf(stderr, "Unsupported node");
  }
}

int main(int argc, char *argv[])
{
  if (argc < 2) {
    fprintf(stderr, "Not enough arguments (expected=2, got=%d)\n", argc);
    return EXIT_FAILURE;
  }

  yyin = (strcmp(argv[1], "-") == 0 ? stdin : fopen(argv[1], "r"));

  if (yyin == NULL) {
    perror(argv[1]);
    return EXIT_FAILURE;
  }

  st_tree *tree = st_tree_init((st_tree *)malloc(sizeof(st_tree)));
  if (0 != yyparse(tree)) {
    fprintf(stderr, "Parse failed\n");
    return EXIT_FAILURE;
  }

  if (tree->root != 0) {
    printf("#include <stdio.h>\n#include <stdlib.h>\n#include <math.h>\n");
    preamble_global(tree->info);
    printf("int main(void)\n{\n");
    preamble(tree->info);
    gen(tree->root);
    printf("return EXIT_SUCCESS;\n}\n");
  }

  st_tree_free(tree);

  return EXIT_SUCCESS;
}
